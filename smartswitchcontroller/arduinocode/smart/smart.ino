const short bt0 = 4; //Raum Knopf
const short bt1 = 5; //Haus Knopf
const short bt2 = 6; //fn0
const short bt3 = 7; //fn1
const short led = 3; //Raum Licht

//Status der Bauteile (Knopf gedrückt ode nicht; LED an oder nicht?)
short bt0std = LOW;
short bt1std = LOW;
short bt2std = LOW;
short bt3std = LOW;
short ledstd = LOW;

String incomingString;
short tmp; //tmp var for comparisons
boolean changesMade = false;
//int msgId = 0;

boolean receiveData() {
  if (Serial.available() > 0) {
    incomingString = Serial.readString();
    incomingString = incomingString.substring(0, incomingString.length() - 1);
    //Serial.println(incomingString);
    return true;
  }
  return false;
}

//Funktion zum umschalten der LED
void toggleLED() {
  if (ledstd == LOW) {
    digitalWrite(led, HIGH);
    ledstd = HIGH;
    sendData("lhon"); 
  } else {
    digitalWrite(led, LOW);
    ledstd = LOW;
    sendData("lhof");
  }
}
//Funktion zum Senden von Daten (z.Z über den Seriellen Port)
void sendData(String data) {
  String outgoingString = "src-id:1dst-id:0msg-id:1";
  //outgoingString += msgId++;
  outgoingString += "msg:";
  outgoingString += data;
  Serial.println(outgoingString);
}

void setup() {
  Serial.begin(9600); //Serielle verbindung starten
  Serial.println("go");
  
  //Funktion der pins festlegen
  pinMode(bt0, INPUT); 
  pinMode(bt1, INPUT);
  pinMode(bt2, INPUT);
  pinMode(bt3, INPUT);
  pinMode(led, OUTPUT);

}

void loop() {
 
  if (receiveData()) {
    if (incomingString == "on") {
      if (ledstd == LOW) {
        toggleLED(); 
      } else {
        sendData("lhon");
      }
    } else if (incomingString == "off") {
      if (ledstd == HIGH) {
        toggleLED(); 
      } else {
        sendData("lhof");
      }
    }
  }
  
  changesMade = false;
  
  tmp = digitalRead(bt0);
  if (tmp != bt0std) {
     if (tmp == HIGH) {
       bt0std = HIGH;
       toggleLED();
       changesMade = true;
     } else {
       bt0std = LOW;
       changesMade = true;
     }
  }
  
  tmp = digitalRead(bt1);
  if (tmp != bt1std) {
     if (tmp == HIGH) {
       sendData("home");
       bt1std = HIGH;
       changesMade = true;
     } else {
       bt1std = LOW;
       changesMade = true;
     }
  }
  
  tmp = digitalRead(bt2);
  if (tmp != bt2std) {
     if (tmp == HIGH) {
       sendData("fn0");
       bt2std = HIGH;
       changesMade = true;
     } else {
       bt2std = LOW;
       changesMade = true;
     }
  }

 
  tmp = digitalRead(bt3);
  if (tmp != bt3std) {
     if (tmp == HIGH) {
       sendData("fn1");
       bt3std = HIGH;
       changesMade = true;
     } else {
       bt3std = LOW;
       changesMade = true;
     }
  }
  
  if (changesMade) delay(100);
}
