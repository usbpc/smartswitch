package com.usbpc.smartswitch.communication;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
//TODO Put all the serial reading into another thread, to read all data even if recive functions are not called fast eough to read all serial data
public class CommunicationHandler {
	
	private InputStream in;
	private OutputStream out;
	private SerialPort serialPort;
	
	private byte[] buffer;
	
	
	public CommunicationHandler() {
		in = null;
		out = null;
	}
	
	//Starts up serial communication
	public boolean start() {
		try {
			//gets CommPortIndentifier based on port name
			//CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier("/dev/tty.usbmodem1421"); //mac version
			CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier("COM3"); //Win version
			
			//tries to open the port for communication (raises exception if port is in use if not blocks the port)
			CommPort commPort = portIdentifier.open("SerialPort",2000);
        	
			//checks if the opend port is a Serial port then casts into SerialPort object
        	if ( commPort instanceof SerialPort ) {
                serialPort = (SerialPort) commPort;
                serialPort.setSerialPortParams(9600,SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE); //Sets the Serial port communication Parameters
                
                //Gets in and output stream from serialPort for easy use.
                in = serialPort.getInputStream();
                out = serialPort.getOutputStream();
                //Creates a StringBuffer with an empty String, empty String is required to use append() methode
                StringBuffer str = new StringBuffer("");
                
                byte[] buffer = new byte[1024];
                int len = -1;
                //Recieves everything until String "go" is found.
                while ( in.available() > -1 ) {  
                	len = in.read(buffer);
                	for (int i = 0; i < len; i++) {
                		str.append((char) buffer[i]);
                	}
                	if (str.indexOf("go") > -1) break;
                }            
            }
        	
		} catch (NoSuchPortException e) {
			System.err.println("Specified Port not found");
			return false;
		} catch (PortInUseException e) {
			System.err.println("Specified Port is currently beeing used");
			return false;
		} catch (UnsupportedCommOperationException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} 
		return true;
	}
	
	//stops serial communication
	public void stop() {
		try {
			//Closes Input and OutputStream used
			in.close();
			out.close();
			//Closes the Serial Port to make it ready to be used by another application
			serialPort.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//sends string over serial
	public void sendString(String str) {
		try {
			out.write(str.getBytes());
			out.flush();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
	//Receives and and returns next line with content from serial
	public String receiveLineAsString() {
		boolean done = false;
		//Creates empty StringBuffer to append() to
		StringBuffer str = new StringBuffer("");
		byte[] buf = new byte[1024];
        int len = -1;
        try {
        	
        	//TODO Clean up code and fix System.lineSeperator() 'bug'
        	if (buffer != null) {
        		for (int i = 0; i < buffer.length; i++) {
					str.append((char) buffer[i]);
					if ((char) buffer[i] == System.lineSeparator().charAt(0)) {
						done = true;
						buffer = new byte[len - i + 1];
						for (int j = 0; j < len - i; j++) {
							buffer[j] = buffer[j + i];
						}
						break;
					}
				}
        		buffer = null;
        	}
        	if (!done) {
				while ( in.available() > -1 ) {
					len = in.read(buf);
					for (int i = 0; i < len; i++) {
						str.append((char) buf[i]);
						if ((char) buf[i] == System.lineSeparator().charAt(0)) {
							done = true;
							buffer = new byte[len - i + 1];
							for (int j = 0; j < len - i; j++) {
								buffer[j] = buf[j + i];
							}
							break;
						}
					}
					if (done) break;
				}
        	}
		} catch (IOException e) {
			e.printStackTrace();
		}
        String nl = (new StringBuffer("")).append((char)10).toString();
        if (str.lastIndexOf(nl) >= str.indexOf(nl) && str.length() < 2) {
        	return null;
        } else {
        	if (str.indexOf(nl) == 0) {
        		str.deleteCharAt(0);
        	}
        	return str.toString();
		}
	}
}