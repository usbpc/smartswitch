package com.usbpc.smartswitch.main;

import com.usbpc.smartswitch.communication.CommunicationHandler;
import com.usbpc.smartswitch.gui.MainWindow;


public class TurnOnLed {
	public static void main(String[] args) {
		
		
		CommunicationHandler comHand = new CommunicationHandler();
		if (comHand.start()) {
			MainWindow mainFrame = new MainWindow("Main Window", comHand);
			mainFrame.setSize(300, 300);
			mainFrame.setVisible(true);
			int counter = 0;
			String str;
			while (true) {
				str = comHand.receiveLineAsString();
				if (str != null) {
					if (System.getProperty("os.name").toLowerCase().indexOf("windows") > -1) {
						mainFrame.appendMessage(counter++ + ": " + str + System.lineSeparator());
					} else {
						mainFrame.appendMessage(counter++ + ": " + str);
					}
				}
			}
		}
		//comHand.stop();
	}
}
