package com.usbpc.smartswitch.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.usbpc.smartswitch.communication.CommunicationHandler;

@SuppressWarnings("serial")
public class MainWindow extends JFrame {
	private JTextArea messages;
	private CommunicationHandler comHand;
	
	public MainWindow(String title, CommunicationHandler com) {
		super(title);
		comHand = com;
		this.setLayout(new BorderLayout());
		
		messages = new JTextArea();
		messages.setEditable(false);
		JScrollPane msg = new JScrollPane(messages, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		this.add(msg, BorderLayout.CENTER);
		
		
		JPanel southFlow = new JPanel();
		southFlow.setLayout(new GridLayout(1, 2));
		this.add(southFlow, BorderLayout.SOUTH);
		
		JButton toggleLed = new JButton("On");
		toggleLed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				comHand.sendString("on");
				}
			});
		southFlow.add(toggleLed);
		
		JButton tm = new JButton("Off");
		tm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				comHand.sendString("off");
			}
		});
		southFlow.add(tm);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void appendMessage(String str) {
		messages.append(str);
		messages.setCaretPosition(messages.getText().length());
		
	}
}
